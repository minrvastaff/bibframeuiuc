# BIBFRAME Project Website

This folder contains the section of code for the BIBFRAME project website at UIUC, along with the script that was used for searching the indexed items.

## Getting Started

To edit the website, simply create a bibframe root folder in your local apache2 server and drop these files in there. Both the website and search are composed with PHP. The version that is running live uses the following linux distro: CentOS 7.4.1708 and PHP 5.4.16 (cli) (built: Nov 15 2017 16:33:54) 

### Prerequisites

Apache and PHP

The MAMP stack is quite useful if you have not turned this on in MacOS, or if you prefer Windows: https://www.mamp.info/en/

Note: There are several options to run a local apache server on your development machine, depending on your OS, and configuration. Original development in this project took place on MacOS, and used the following instructions to setup the Apache server: https://coolestguidesontheplanet.com/get-apache-mysql-php-and-phpmyadmin-working-on-macos-sierra/ 

## Deployment

Note that the code here for search points at a Google Custom Search Engine. The Custom search engine can be configured with site map URLs for indexing. The sitemap XML generation code is available here: https://bitbucket.org/minrvaproject-admin/bibframeuiuc/src/7863b737d7dc51f77d3ebcb5215228b6d8082825/sitemaps/sitemap.py?at=master&fileviewer=file-view-default


## Authors

* **Jim Hahn** - *Initial work* - (https://github.com/jimfhahn)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

