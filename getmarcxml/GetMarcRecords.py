#uses python 2.7
__author__ = 'sarahnagle'
import pycurl
import csv
from StringIO import StringIO
import os

def create_files(m, z):
    file_name = m + "_marc.xml"
    marc_file = open(os.path.join("/Users/sarahnagle/Desktop/MarcFiles3", file_name), "w") #Put folder location to save marcxml files here. You will have to create the empty folder first.
    marc_file.write(z)
    marc_file.close()

with open ('/Users/sarahnagle/Desktop/BibIDs/bibId3.csv', 'rb') as x: # Put list of BibIDs (output from check_URLS.py) to retrieve here.
    reader = csv.reader(x, delimiter=" ")
    for id_list in reader:
        pass
error_list = []
for x in id_list:
    buffer = StringIO()
    c = pycurl.Curl()
    j = 'http://quest.library.illinois.edu/GetMARC/one.aspx/' + x + '.marc'
    c.setopt(c.URL, j)
    c.setopt(c.WRITEFUNCTION, buffer.write)
    c.perform()
    r = buffer.getvalue()
    print r
    try:
        create_files(x, r)
    except:
        print "ERROR!"
        error_list.append(x)
        continue
print error_list